package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.service.DamageListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 商品报损
 */
@RestController
@RequestMapping("/damageListGoods")
public class DamageListGoodsController {

    @Autowired
    private DamageListGoodsService damageListGoodsService;


    /**
     * 保存报损单
     * 请求URL：http://localhost:8080/damageListGoods/save?damageNumber=BS1605766644460（报损单号,前端生成）
     * 请求参数：DamageList damageList, String damageListGoodsStr
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：ServiceVO
     */
    @PostMapping("/save")
    public ServiceVO save(DamageList damageList, String damageListGoodsStr) {
        return damageListGoodsService.save(damageList, damageListGoodsStr);
    }


    /**
     * 报损单查询
     * 请求URL：http://localhost:8080/damageListGoods/list
     * 请求参数：String  sTime（开始时间）, String  eTime（结束时间）
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：Map<String,Object>
     */
    @PostMapping("list")
    public Map<String, Object> list(String sTime, String eTime) {
        return damageListGoodsService.list(sTime, eTime);
    }


    /**
     * 查询报损单商品信息
     * 请求URL：http://localhost:8080/damageListGoods/goodsList
     * 请求参数：Integer damageListId（报损单Id）
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：Map<String,Object>
     */
    @PostMapping("goodsList")
    public Map<String, Object> goodsList(Integer damageListId) {
        return damageListGoodsService.goodsList(damageListId);
    }
}
