package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.GoodsService;
import org.apache.ibatis.annotations.Param;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 商品信息Controller
 */
@RestController
@RequestMapping("/goods")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;


    /**
     * 分页查询商品库存信息  首页
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param codeOrName  商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @RequestMapping("/listInventory")
    @RequiresPermissions(value = "当前库存查询")
    public Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        return goodsService.listInventory(page, rows, codeOrName, goodsTypeId);
    }

    /**
     * 查询所有商品信息查询所有商品信息
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param goodsName   商品名称
     * @param goodsTypeId 商品类别ID
     *                    请求URL：http://localhost:8080/goods/list
     *                    请求参数：Integer page, Integer rows, String goodsName, Integer goodsTypeId
     *                    请求方式：POST
     *                    返回值类型：JSON
     *                    返回值：Map<String,Object>请求URL：http://localhost:8080/goods/list
     *                    请求参数：Integer page, Integer rows, String goodsName, Integer goodsTypeId
     *                    请求方式：POST
     *                    返回值类型：JSON
     *                    返回值：Map<String,Object>
     */
    @PostMapping("/list")
    public Map<String, Object> list(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {

        return this.listInventory(page, rows, goodsName, goodsTypeId);
    }

    /**
     * 生成商品编码
     *
     * @return
     */
    @RequestMapping("/getCode")
    @RequiresPermissions(value = "商品管理")
    public ServiceVO getCode() {
        return goodsService.getCode();
    }

    /**
     * 添加或修改商品信息
     *
     * @param goods 商品信息实体
     *              请求URL：http://localhost:8080/goods/save?goodsId=37
     *              请求参数：Goods goods
     *              请求方式：POST
     *              返回值类型：JSON
     *              返回值：ServiceVO
     */
    @PostMapping("/save")
    public ServiceVO save(Goods goods) {
        ServiceVO serviceVO = goodsService.save(goods);
        return serviceVO;
    }

    /**
     * 删除商品信息要判断商品状态,入库、有进货和销售单据的不能删除）
     *
     * @param goodsId 商品ID
     *                请求URL：http://localhost:8080/goods/delete
     *                请求参数：Integer goodsId
     *                请求方式：POST
     *                返回值类型：JSON
     *                返回值：ServiceVO
     */
    @PostMapping("/delete")
    public ServiceVO delete(Integer goodsId) {
        ServiceVO serviceVO = goodsService.delete(goodsId);
        return serviceVO;
    }

    /**
     * 无库存商品列表展示（可以根据商品名称或编码查询）
     *
     * @param page       当前页
     * @param rows       每页显示条数
     * @param nameOrCode 商品名称或商品编码
     *                   请求URL：http://localhost:8080/goods/getNoInventoryQuantity
     *                   请求参数：Integer page,Integer rows,String nameOrCode
     *                   请求方式：POST
     *                   返回值类型：JSON
     *                   返回值：Map<String,Object>
     */
    @PostMapping("/getNoInventoryQuantity")
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        return goodsService.getNoInventoryQuantity(page, rows, nameOrCode);
    }


    /**
     * 有库存商品列表展示（可以根据商品名称或编码查询）
     *
     * @param page       当前页
     * @param rows       每页显示条数
     * @param nameOrCode 商品名称或商品编码
     *                   请求URL：http://localhost:8080/goods/getHasInventoryQuantity
     *                   请求参数：Integer page,Integer rows,String nameOrCode
     *                   请求方式：POST
     *                   返回值类型：JSON
     *                   返回值：Map<String,Object>
     */
    @PostMapping("/getHasInventoryQuantity")
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        return goodsService.getHasInventoryQuantity(page, rows, nameOrCode);
    }


    /**
     * 添加库存、修改数量或成本价
     *
     * @param goodsId           商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice   成本价
     *                          请求URL：http://localhost:8080/goods/saveStock?goodsId=25
     *                          请求参数：Integer goodsId,Integer inventoryQuantity,double purchasingPrice
     *                          请求方式：POST
     *                          返回值类型：JSON
     *                          返回值：ServiceVO
     */
    @PostMapping("/saveStock")
    public ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        return goodsService.saveStock(goodsId, inventoryQuantity, purchasingPrice);
    }

    /**
     * 删除库存（要判断商品状态 入库、有进货和销售单据的不能删除）
     *
     * @param goodsId 商品ID
     *                请求URL：http://localhost:8080/goods/deleteStock
     *                请求参数：Integer goodsId
     *                请求方式：POST
     *                返回值类型：JSON
     *                返回值：ServiceVO
     */
    @PostMapping("/deleteStock")
    public ServiceVO deleteStock(Integer goodsId) {

        return goodsService.delete(goodsId);
    }

    /**
     * 查询库存报警商品信息 查询所有当前库存量小于库存下限的商品信息
     * 请求URL：http://localhost:8080/goods/listAlarm
     * 请求参数：无
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：Map<String,Object>
     */
    @PostMapping("/listAlarm")
    public Map<String, Object> listAlarm() {
        return goodsService.listAlarm();
    }

}
