package com.atguigu.jxc.controller;


import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.service.GoodsTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 商品分类管理
 */
@RestController
@RequestMapping("/goodsType")
public class GoodsTypeController {
    @Autowired
    private GoodsTypeService goodsTypeService;

    /**
     * 查询商品所有分类
     * 请求URL：http://localhost:8080/goodsType/loadGoodsType
     * 请求参数：无
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：String
     */
    @RequestMapping("/loadGoodsType")
    public String loadGoodsType() {
        return goodsTypeService.loadGoodsType();
    }

    /**
     * 新增分类
     * 请求URL：http://localhost:8080/goodsType/save
     * 请求参数：String  goodsTypeName,Integer  pId
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：ServiceVO
     */
    @PostMapping("/save")
    public ServiceVO save(String goodsTypeName, Integer pId) {

        ServiceVO serviceVO = goodsTypeService.save(goodsTypeName, pId);

        return serviceVO;
    }

    /**
     * 删除分类
     * 请求URL：http://localhost:8080/goodsType/delete
     * 请求参数：Integer  goodsTypeId
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：ServiceVO
     */
    @PostMapping("/delete")
    public ServiceVO delete(Integer goodsTypeId) {
        ServiceVO serviceVO = goodsTypeService.delete(goodsTypeId);
        return serviceVO;
    }
}
