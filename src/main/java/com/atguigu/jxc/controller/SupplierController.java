package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 供应商Controller控制器！
 */

@Controller
@RequestMapping("/supplier")
public class SupplierController {

    @Autowired
    SupplierService supplierService;

    /**
     * 分页查询供应商信息
     *
     * @param page         当前页数！
     * @param rows         每页显示的记录数！
     * @param supplierName 供应商公司的名称！
     * @return
     */

    @PostMapping("/list")
    @ResponseBody
    public Map<String, Object> list(Integer page, Integer rows, String supplierName) {
        return supplierService.list(page, rows, supplierName);
    }

    /**
     * 新增 与 修改 供应商
     *
     * @param supplier
     * @return
     */
    @PostMapping("/save")
    @ResponseBody
    public ServiceVO saveSupplier(Supplier supplier, String supplierId) {
        // 调用服务层中的方法！
        return supplierService.saveSupplier(supplier, supplierId);

    }

    /**
     * 删除供应商信息（可以批量删除）
     * @param ids
     * @return
     */
    @PostMapping("/delete")
    @ResponseBody
    public ServiceVO delete(String ids) {
        // 调用服务层方法
        supplierService.deleteSupplierById(ids);
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }
}
