package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * 客户信息 DAO
 */
@Repository
public interface CustomerDao {


    /**
     * 获取客户列表分页集合
     *
     * @param offSet       页码
     * @param rows         条数
     * @param customerName 客户名称
     * @return 客户信息
     */
    List<Customer> getCustomerList(@Param("offSet") int offSet, @Param("rows") Integer rows, @Param("customerName") String customerName);


    /**
     * 获取客户总数
     *
     * @return long
     */
    Long getCustomerCount();


    /**
     * 保存客户信息
     *
     * @param customerName 客户名称
     * @param contacts     联络方式
     * @param phoneNumber  客户手机号
     * @param address      住址
     * @param remarks      评论
     * @return 0否，1是
     */
    int save(@Param("customerName") String customerName, @Param("contacts") String contacts, @Param("phoneNumber") String phoneNumber, @Param("address") String address, @Param("remarks") String remarks);


    /**
     * 修改客户信息
     *
     * @param customerName 客户名称
     * @param contacts     联络方式
     * @param phoneNumber  客户手机号
     * @param address      住址
     * @param remarks      评论
     * @param customerId   客户id
     * @return 0否，1是
     */
    int updateById(@Param("customerName") String customerName, @Param("contacts") String contacts, @Param("phoneNumber") String phoneNumber, @Param("address") String address, @Param("remarks") String remarks, @Param("customerId") Integer customerId);


    /**
     * 根据客户ID删除客户信息
     *
     * @param integer 客户Id
     * @return 0否，1是
     */
    int deleteById(List<Integer> integer);
}
