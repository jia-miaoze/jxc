package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DamageListDao {

    // 添加数据
    int save(@Param("damageNumber") String damageNumber, @Param("damageDate") String damageDate, @Param("remarks") String remarks, @Param("userId") Integer userId);

    // 查询刚插入数据的ID
    Integer selectOne(@Param("damageNumber") String damageNumber, @Param("damageDate") String damageDate, @Param("remarks") String remarks, @Param("userId") Integer userId);

    //String  sTime（开始时间）, String  eTime（结束时间）
    List<DamageList> selectByDate(@Param("sTime") String sTime, @Param("eTime") String eTime);
}
