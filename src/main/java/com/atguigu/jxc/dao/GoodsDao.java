package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 商品信息
 */
@Repository
public interface GoodsDao {

    // 获取当前商品最大编码
    String getMaxCode();

    // 获取商品集合
    List<Goods> getGoodsInventoryList(@Param("offSet") int offSet, @Param("rows") Integer rows, @Param("codeOrName") String codeOrName, @Param("goodsTypeId") Integer goodsTypeId);

    // 获取总条数
    Long getGoodsInventoryCount();

    // 添加商品信息
    int save(@Param("goodsCode") String goodsCode, @Param("goodsName") String goodsName, @Param("goodsModel") String goodsModel, @Param("goodsUnit") String goodsUnit, @Param("purchasingPrice") double purchasingPrice, @Param("sellingPrice") double sellingPrice, @Param("minNum") Integer minNum, @Param("goodsProducer") String goodsProducer, @Param("remarks") String remarks, @Param("goodsTypeId") Integer goodsTypeId, @Param("inventoryQuantity") Integer inventoryQuantity, @Param("lastPurchasingPrice") double lastPurchasingPrice, @Param("state") Integer state);

    // 修改商品信息
    int updateById(@Param("goodsCode") String goodsCode, @Param("goodsName") String goodsName, @Param("goodsModel") String goodsModel, @Param("goodsUnit") String goodsUnit, @Param("purchasingPrice") double purchasingPrice, @Param("sellingPrice") double sellingPrice, @Param("minNum") Integer minNum, @Param("goodsProducer") String goodsProducer, @Param("remarks") String remarks, @Param("goodsTypeId") Integer goodsTypeId, @Param("goodsId") Integer goodsId, @Param("inventoryQuantity") Integer inventoryQuantity, @Param("lastPurchasingPrice") double lastPurchasingPrice, @Param("state") Integer state);

    // 根据ID查询一条信息
    Goods selectById(@Param("goodsId") Integer goodsId);

    // 根据ID删除一条信息
    int delete(@Param("goodsId") Integer goodsId);

    // 添加库存、修改数量或成本价
    int updateStockById(@Param("purchasingPrice") double purchasingPrice, @Param("goodsId") Integer goodsId, @Param("inventoryQuantity") Integer inventoryQuantity);

    // 返回Goods所以集合
    List<Goods> selectGoodsList();

    int updateLastPurchasingPrice(@Param("lastPurchasingPrice") double lastPurchasingPrice, @Param("goodsId") Integer goodsId);


    void updateById(Goods goods);
}
