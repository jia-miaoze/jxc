package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.Menu;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 商品类别
 */
@Repository
public interface GoodsTypeDao {


    Integer updateGoodsTypeState(GoodsType parentGoodsType);

    // 根据菜单父ID获取当前角色下的菜单信息
    GoodsType getGoodsTypeByParentId(@Param("parentId") Integer parentId);

    //   添加分类
    int save(@Param("goodsTypeName") String goodsTypeName, @Param("pId") Integer pId, @Param("goodsTypeState") Integer goodsTypeState);

    //   删除分类
    int delete(Integer goodsTypeId);


    List<GoodsType> getGoodsTypeByParentId(@Param("parentId") Integer parentId, @Param("roleId") Integer roleId);

    GoodsType getGoodsTypeStateBypId(Integer pId);

    GoodsType selectByGoodsId(Integer goodsTypeId);

    // 查询有几个子类
    Integer selectCountByPId(Integer pId);
}
