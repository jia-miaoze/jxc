package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowListGoods;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OverflowListGoodsDao {
    int save(@Param("goodsId")Integer goodsId, @Param("goodsCode")String goodsCode,
             @Param("goodsName")String goodsName, @Param("goodsModel")String goodsModel,
             @Param("goodsUnit")String goodsUnit, @Param("goodsNum")Integer goodsNum,
             @Param("price")double price, @Param("total") double total,
             @Param("overflowListId")Integer overflowListId, @Param("goodsTypeId")Integer goodsTypeId);

    List<OverflowListGoods> selectById(Integer overflowListId);
}
