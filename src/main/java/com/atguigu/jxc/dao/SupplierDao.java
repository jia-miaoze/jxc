package com.atguigu.jxc.dao;


import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface SupplierDao {

    // 根据条件获取供应商列表
    List<Supplier> getSupplierList(@Param("offSet") Integer offSet, @Param("rows") Integer rows, @Param("supplierName") String supplierName);

    // 根据条件获取供应商列表的数量
    Integer getSupplierCount(@Param("supplierName") String supplierName);

    // 新增供应商信息
    void insertSupplier(Supplier supplier);

    // 根据角色名称查找角色
    Supplier findByName(@Param("supplierName") String supplierName);

    // 根据id更改信息
    void updateSupplier(Supplier supplier);

    // 根据id查询供应商信息
    Supplier getById(@Param("ids") String ids);

    // 根据id 删除供应商信息
    void deleteSupplierById(@Param("split") String[] split);


}
