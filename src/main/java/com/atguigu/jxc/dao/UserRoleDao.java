package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.UserRole;
import org.springframework.stereotype.Repository;

/**
 * 用户角色
 */
@Repository
public interface UserRoleDao {

    // 根据用户id删除用户角色
    Integer deleteUserRoleByUserId(Integer userId);

    // 为用户添加角色
    Integer addUserRole(UserRole userRole);
}
