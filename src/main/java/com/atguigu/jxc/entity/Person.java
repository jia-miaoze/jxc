package com.atguigu.jxc.entity;

import java.util.Date;

public class Person {
    private String name; //姓名
    private String number; //身份证号
    private Date birthday; //出生日期
    
   /************Begin*************/
    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Person(String name, String number, Date birthday) {
        this.name = name;
        this.number = number;
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", number='" + number + '\'' +
                ", birthday=" + birthday +
                '}';

    }

    public Person() {
    }
/************End*************/
}
