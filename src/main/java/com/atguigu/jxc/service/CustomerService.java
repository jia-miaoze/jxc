package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;

import java.util.Map;


/**
 * 客户信息服务 Service
 */
public interface CustomerService {


    /**
     * 获取客户列表分页集合
     * 名称模糊查询
     *
     * @param page         页码
     * @param rows         条数
     * @param customerName 客户名称
     * @return 客户信息
     */
    Map<String, Object> list(Integer page, Integer rows, String customerName);


    /**
     * 保存或修该客户信息
     *
     * @param customer 客户信息
     * @return 0否，1是
     */
    ServiceVO save(Customer customer);


    /**
     * 根据客户ID删除客户信息
     *
     * @param ids 客户Id
     * @return 0否，1是
     */
    ServiceVO delete(String ids);
}
