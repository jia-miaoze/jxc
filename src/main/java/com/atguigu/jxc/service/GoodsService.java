package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

public interface GoodsService {

    //生成商品编码
    ServiceVO getCode();

    //查询所有商品信息查询所有商品信息
    Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId);

    //  添加或修改商品信息
    ServiceVO save(Goods goods);

    // 删除商品信息要判断商品状态,入库、有进货和销售单据的不能删除）
    ServiceVO delete(Integer goodsId);

    //无库存商品列表展示（可以根据商品名称或编码查询）
    Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    // 有库存商品列表展示（可以根据商品名称或编码查询）
    Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    //添加库存、修改数量或成本价
    ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice);

    //查询所有当前库存量小于库存下限的商品信息
    Map<String, Object> listAlarm();

}
