package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;


/**
 * 商品类型管理服务
 */
public interface GoodsTypeService {
    /**
     * 查询商品所有分类
     *
     * @return 商品类型
     */
    String loadGoodsType();

    /**
     * 添加商品类型分类
     *
     * @param goodsTypeName 商品类型名称
     * @param pId           分类id
     * @return 业务执行状态信息
     */
    ServiceVO save(String goodsTypeName, Integer pId);


    /**
     * 删除商品类型分类
     *
     * @param goodsTypeId 分类id
     * @return 业务执行状态信息
     */
    ServiceVO delete(Integer goodsTypeId);


}
