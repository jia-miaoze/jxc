package com.atguigu.jxc.service;

import javax.servlet.http.HttpSession;

/**
 *
 */
public interface MenuService {

    String loadMenu(HttpSession session);

    String loadCheckMenu(Integer roleId);

}
