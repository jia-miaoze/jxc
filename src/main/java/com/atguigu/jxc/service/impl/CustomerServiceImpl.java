package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.CustomerService;
import com.atguigu.jxc.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 客户信息服务 Service
 */
@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;
    @Autowired
    private LogService logService;


    /**
     * 根据客户ID删除客户信息
     *
     * @param ids 客户Id
     * @return 0否，1是
     */
    @Override
    public ServiceVO delete(String ids) {
        ServiceVO serviceVO;
        String[] split = ids.split(",");
        List<Integer> list = new ArrayList<>();

        for (String s : split) {
            list.add(Integer.parseInt(s));
        }

        int number = customerDao.deleteById(list);
        if (number != 1) {
            return new ServiceVO(ErrorCode.REQ_ERROR_CODE, ErrorCode.REQ_ERROR_MESS);
        }

        serviceVO = new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
        logService.save(new Log(Log.DELETE_ACTION, "客户删除"));
        return serviceVO;
    }


    /**
     * 保存或修该客户信息
     *
     * @param customer 客户信息
     * @return 0否，1是
     */
    @Override
    public ServiceVO save(Customer customer) {
        ServiceVO serviceVO;
        String customerName = customer.getCustomerName();
        String contacts = customer.getContacts();
        String phoneNumber = customer.getPhoneNumber();
        String address = customer.getAddress();
        String remarks = customer.getRemarks();
        Integer customerId = customer.getCustomerId();
        if (customerId == null) {
            int number = customerDao.save(customerName, contacts, phoneNumber, address, remarks);
            if (number == 1) {
                serviceVO = new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
            } else {
                serviceVO = new ServiceVO(ErrorCode.REQ_ERROR_CODE, ErrorCode.REQ_ERROR_MESS);
            }
        } else {
            int number = customerDao.updateById(customerName, contacts, phoneNumber, address, remarks, customerId);
            if (number == 1) {
                serviceVO = new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
            } else {
                serviceVO = new ServiceVO(ErrorCode.REQ_ERROR_CODE, ErrorCode.REQ_ERROR_MESS);
            }
        }
        logService.save(new Log(Log.UPDATE_ACTION, "客户添加或修改"));
        return serviceVO;
    }


    /**
     * 获取客户列表分页集合
     *
     * @param page         页码
     * @param rows         条数
     * @param customerName 客户名称
     * @return 客户信息
     */
    @Override
    public Map<String, Object> list(Integer page, Integer rows, String customerName) {
        Map<String, Object> map = new HashMap<>();
//     当前页
        page = page == 0 ? 1 : page;
//        当前页从第几条开始
        int offSet = (page - 1) * rows;

        List<Customer> customerList = customerDao.getCustomerList(offSet, rows, customerName);
        Long count = customerDao.getCustomerCount();
        map.put("rows", customerList);

        map.put("total", count);
        logService.save(new Log(Log.SELECT_ACTION, "客户列表分页"));
        return map;
    }
}
