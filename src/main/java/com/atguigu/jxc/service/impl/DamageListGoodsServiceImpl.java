package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageListDao;
import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.DamageListGoodsService;
import com.atguigu.jxc.service.LogService;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class DamageListGoodsServiceImpl implements DamageListGoodsService {
    @Autowired
    private DamageListDao damageListDao;
    @Autowired
    private DamageListGoodsDao damageListGoodsDao;
    @Autowired
    private GoodsDao goodsDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private LogService logService;

    //   保存报损单
    @Override
    public ServiceVO save(DamageList damageList, String damageListGoodsStr) {
        ServiceVO serviceVO;
        String damageNumber = damageList.getDamageNumber();
        String damageDate = damageList.getDamageDate();
        String remarks = damageList.getRemarks();
        damageList.setUserId(1);
        Integer userId = damageList.getUserId();
        int damageListNumber = damageListDao.save(damageNumber, damageDate, remarks, userId);
        logService.save(new Log(Log.INSERT_ACTION, "增加damageList"));
        if (damageListNumber == 1) {
            serviceVO = new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
        } else {
            return new ServiceVO(ErrorCode.REQ_ERROR_CODE, ErrorCode.REQ_ERROR_MESS);
        }
        Integer damageListId = damageListDao.selectOne(damageNumber, damageDate, remarks, userId);
        List<Map> mapList = JSON.parseArray(damageListGoodsStr, Map.class);
        if (!CollectionUtils.isEmpty(mapList)) {
            //  循环遍历
            for (Map map : mapList) {
                Integer goodsId = (Integer) map.get("goodsId");
                String goodsCode = (String) map.get("goodsCode");
                String goodsName = (String) map.get("goodsName");
                String goodsModel = (String) map.get("goodsModel");
                String goodsUnit = (String) map.get("goodsUnit");
                double lastPurchasingPrice = Double.parseDouble(String.valueOf(map.get("lastPurchasingPrice")));
                Double price = Double.parseDouble(String.valueOf(map.get("price")));
                Integer goodsNum = Integer.parseInt(String.valueOf(map.get("goodsNum")));
                Double total = Double.parseDouble(String.valueOf(map.get("total")));
                Goods goods = goodsDao.selectById(goodsId);
                Integer goodsTypeId = goods.getGoodsTypeId();
                int damageListGoodsNumber = damageListGoodsDao.save(goodsId, goodsCode, goodsName, goodsModel, goodsUnit, goodsNum, price, total, damageListId, goodsTypeId);
                logService.save(new Log(Log.INSERT_ACTION, "增加damageListGoods"));
                if (damageListGoodsNumber == 1) {
                    serviceVO = new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
                } else {
                    return new ServiceVO(ErrorCode.REQ_ERROR_CODE, ErrorCode.REQ_ERROR_MESS);
                }
                int updateNumber = goodsDao.updateLastPurchasingPrice(lastPurchasingPrice, goodsId);
                logService.save(new Log(Log.INSERT_ACTION, "updateLastPurchasingPrice"));
                if (updateNumber == 1) {
                    serviceVO = new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
                } else {
                    return new ServiceVO(ErrorCode.REQ_ERROR_CODE, ErrorCode.REQ_ERROR_MESS);
                }
            }
        }

        return serviceVO;
    }

    //报损单查询
    @Override
    public Map<String, Object> list(String sTime, String eTime) {
        HashMap<String, Object> map = new HashMap<>();
        ArrayList<DamageList> damageLists = new ArrayList<>();
        List<DamageList> damageListAll = damageListDao.selectByDate(sTime, eTime);
        for (DamageList damageList : damageListAll) {
            User userById = userDao.getUserById(damageList.getUserId());
            damageList.setTrueName(userById.getTrueName());
            damageLists.add(damageList);
        }
        map.put("rows", damageLists);
        logService.save(new Log(Log.SELECT_ACTION, "报损单查询"));
        return map;
    }

    //查询报损单商品信息
    @Override
    public Map<String, Object> goodsList(Integer damageListId) {
        HashMap<String, Object> map = new HashMap<>();
        List<DamageListGoods> damageListGoodsList = damageListGoodsDao.selectById(damageListId);
        map.put("rows", damageListGoodsList);
        logService.save(new Log(Log.SELECT_ACTION, "查询报损单商品信息"));
        //damageListGoodsDao
        return map;
    }
}
