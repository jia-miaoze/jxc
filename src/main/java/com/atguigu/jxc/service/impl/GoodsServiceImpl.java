package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SaleListGoodsService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;
    @Autowired
    private LogService logService;
    @Autowired
    private SaleListGoodsService saleListGoodsService;
    @Autowired
    private CustomerReturnListGoodsService customerReturnListGoodsService;

    // 封装 获取Goods集合
    private List<Goods> selectGoodsList(Integer page, Integer rows, String nameOrCode, Integer goodsTypeId) {

//     当前页
        page = page == 0 ? 1 : page;
//        当前页从第几条开始
        int offSet = (page - 1) * rows;
        return goodsDao.getGoodsInventoryList(offSet, rows, nameOrCode, goodsTypeId);
    }

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        int intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        StringBuilder unitCode = new StringBuilder(Integer.toString(intCode));

        for (int i = 4; i > Integer.toString(intCode).length(); i--) {

            unitCode.insert(0, "0");

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode.toString());
    }

    /**
     * @param page        当前页
     * @param rows        每页显示条数
     * @param codeOrName  商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @Override
    public Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        Map<String, Object> map = new HashMap<>();
        List<Goods> goodsList = this.selectGoodsList(page, rows, codeOrName, goodsTypeId);
        for (Goods goods : goodsList) {
            // 销售总量等于销售单据的销售数据减去退货单据的退货数据
            goods.setSaleTotal(saleListGoodsService.getSaleTotalByGoodsId(goods.getGoodsId())
                    - customerReturnListGoodsService.getCustomerReturnTotalByGoodsId(goods.getGoodsId()));

            goods.setGoodsTypeName(goods.getGoodsType().getGoodsTypeName());
            goods.setGoodsTypeId(goods.getGoodsType().getGoodsTypeId());

        }
        Long count = goodsDao.getGoodsInventoryCount();
        map.put("rows", goodsList);

        map.put("total", count);

        logService.save(new Log(Log.SELECT_ACTION, "分页查询商品库存信息"));

        return map;
    }

    // 添加或修改商品信息
    @Override
    public ServiceVO save(Goods goods) {
        ServiceVO serviceVO;
        String goodsCode = goods.getGoodsCode();//商品编号
        String goodsName = goods.getGoodsName();// 商品名称
        String goodsModel = goods.getGoodsModel();//商品型号
        String goodsUnit = goods.getGoodsUnit();//商品单位
        double purchasingPrice = goods.getPurchasingPrice();//采购价格
        double sellingPrice = goods.getSellingPrice();//销售价格
        Integer minNum = goods.getMinNum();//库存下限
        String goodsProducer = goods.getGoodsProducer();//生成厂商
        String remarks = goods.getRemarks();//备注
        // String goodsTypeName = goods.getGoodsTypeName();//商品类别
        Integer goodsTypeId = goods.getGoodsTypeId();//商品类别id
        goods.setInventoryQuantity(0);
        Integer inventoryQuantity = goods.getInventoryQuantity();//库存数量
        goods.setLastPurchasingPrice(purchasingPrice);
        double lastPurchasingPrice = goods.getLastPurchasingPrice();
        goods.setState(2);
        Integer state = goods.getState();
        goods.setInventoryQuantity(0);
        Integer goodsId = goods.getGoodsId();
        if (goodsId == null) {
            int number = goodsDao.save(goodsCode, goodsName, goodsModel, goodsUnit, purchasingPrice, sellingPrice, minNum, goodsProducer, remarks, goodsTypeId, inventoryQuantity, lastPurchasingPrice, state);
            if (number == 1) {
                serviceVO = new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
            } else {
                serviceVO = new ServiceVO(ErrorCode.REQ_ERROR_CODE, ErrorCode.REQ_ERROR_MESS);
            }
            logService.save(new Log(Log.INSERT_ACTION, "商品添加"));
        } else {
            int number = goodsDao.updateById(goodsCode, goodsName, goodsModel, goodsUnit, purchasingPrice, sellingPrice, minNum, goodsProducer, remarks, goodsTypeId, goodsId, inventoryQuantity, lastPurchasingPrice, state);
            if (number == 1) {
                serviceVO = new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
            } else {
                serviceVO = new ServiceVO(ErrorCode.REQ_ERROR_CODE, ErrorCode.REQ_ERROR_MESS);
            }
            logService.save(new Log(Log.UPDATE_ACTION, "供应商添加"));
        }

        return serviceVO;
    }

    //  删除商品信息要判断商品状态,入库、有进货和销售单据的不能删除）
    @Override
    public ServiceVO delete(Integer goodsId) {
        ServiceVO serviceVO;
        Goods goods = goodsDao.selectById(goodsId);
        Integer state = goods.getState();
        if (state == 0) {
            int number = goodsDao.delete(goodsId);
            if (number == 1) {
                serviceVO = new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
            } else {
                return new ServiceVO(ErrorCode.REQ_ERROR_CODE, ErrorCode.REQ_ERROR_MESS);
            }
        } else {
            return new ServiceVO(ErrorCode.REQ_ERROR_CODE, ErrorCode.REQ_ERROR_MESS);
        }
        logService.save(new Log(Log.DELETE_ACTION, "商品删除"));
        return serviceVO;
    }


    //无库存商品列表展示（可以根据商品名称或编码查询）
    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String, Object> map = new HashMap<>();
        List<Goods> goods = selectGoodsList(page, rows, nameOrCode, null);
        ArrayList<Goods> goodsArrayList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(goods)) {
            for (Goods good : goods) {
                Integer inventoryQuantity = good.getInventoryQuantity();
                if (inventoryQuantity == 0) {
                    // 销售总量等于销售单据的销售数据减去退货单据的退货数据
                    good.setSaleTotal(saleListGoodsService.getSaleTotalByGoodsId(good.getGoodsId())
                            - customerReturnListGoodsService.getCustomerReturnTotalByGoodsId(good.getGoodsId()));
                    good.setGoodsTypeName(good.getGoodsType().getGoodsTypeName());
                    good.setGoodsTypeId(good.getGoodsType().getGoodsTypeId());
                    goodsArrayList.add(good);
                }
                Long count = goodsDao.getGoodsInventoryCount();
                map.put("rows", goodsArrayList);
                map.put("total", count);
                logService.save(new Log(Log.SELECT_ACTION, "无库存商品列表展示（可以根据商品名称或编码查询）"));
            }
        }
        return map;
    }

    // 有库存商品列表展示（可以根据商品名称或编码查询）
    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String, Object> map = new HashMap<>();
        List<Goods> goods = selectGoodsList(page, rows, nameOrCode, null);
        ArrayList<Goods> goodsArrayList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(goods)) {
            for (Goods good : goods) {
                Integer inventoryQuantity = good.getInventoryQuantity();
                if (inventoryQuantity != 0) {
                    // 销售总量等于销售单据的销售数据减去退货单据的退货数据
                    good.setSaleTotal(saleListGoodsService.getSaleTotalByGoodsId(good.getGoodsId())
                            - customerReturnListGoodsService.getCustomerReturnTotalByGoodsId(good.getGoodsId()));
                    good.setGoodsTypeName(good.getGoodsType().getGoodsTypeName());
                    good.setGoodsTypeId(good.getGoodsType().getGoodsTypeId());
                    goodsArrayList.add(good);
                }
                Long count = goodsDao.getGoodsInventoryCount();
                map.put("rows", goodsArrayList);
                map.put("total", count);
                logService.save(new Log(Log.SELECT_ACTION, "有库存商品列表展示（可以根据商品名称或编码查询）"));
            }
        }
        return map;
    }

    //    添加库存、修改数量或成本价
    @Override
    public ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        ServiceVO serviceVO;
        int number = goodsDao.updateStockById(purchasingPrice, goodsId, inventoryQuantity);
        if (number == 1) {
            serviceVO = new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
        } else {
            serviceVO = new ServiceVO(ErrorCode.REQ_ERROR_CODE, ErrorCode.REQ_ERROR_MESS);
        }
        logService.save(new Log(Log.UPDATE_ACTION, "添加库存、修改数量或成本价"));


        return serviceVO;

    }
//    查询所有当前库存量小于库存下限的商品信息

    @Override
    public Map<String, Object> listAlarm() {
        HashMap<String, Object> map = new HashMap<>();
        ArrayList<Goods> goodsList = new ArrayList<>();
        List<Goods> goodsListAll = goodsDao.selectGoodsList();
        for (Goods goods : goodsListAll) {
            Integer inventoryQuantity = goods.getInventoryQuantity();
            Integer minNum = goods.getMinNum();
            if (inventoryQuantity < minNum) {
                // 销售总量等于销售单据的销售数据减去退货单据的退货数据
                goods.setSaleTotal(saleListGoodsService.getSaleTotalByGoodsId(goods.getGoodsId())
                        - customerReturnListGoodsService.getCustomerReturnTotalByGoodsId(goods.getGoodsId()));
                goods.setGoodsTypeName(goods.getGoodsType().getGoodsTypeName());
                goods.setGoodsTypeId(goods.getGoodsType().getGoodsTypeId());
                goodsList.add(goods);
            }
        }
        Long count = goodsDao.getGoodsInventoryCount();
        map.put("rows", goodsList);
        map.put("total", count);
        logService.save(new Log(Log.SELECT_ACTION, "查询所有当前库存量小于库存下限的商品信息"));
        return map;
    }
}
