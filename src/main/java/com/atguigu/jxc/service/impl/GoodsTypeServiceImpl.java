package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsTypeDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.GoodsTypeService;
import com.atguigu.jxc.service.LogService;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class GoodsTypeServiceImpl implements GoodsTypeService {
    @Autowired
    private GoodsTypeDao goodsTypeDao;
    @Autowired
    private LogService logService;

    //查询商品所有分类
    @Override
    public String loadGoodsType() {
        logService.save(new Log(Log.SELECT_ACTION, "查询商品所有分类"));
        return String.valueOf(this.getAllMenu(-1, 1));
    }

    //  增加分类
    @Override
    public ServiceVO save(String goodsTypeName, Integer pId) {
        ServiceVO serviceVO;
        Integer goodsTypeState = 0;
        int number = goodsTypeDao.save(goodsTypeName, pId, goodsTypeState);
        logService.save(new Log(Log.INSERT_ACTION, "增加分类"));
        if (number == 1) {
            serviceVO = new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
            GoodsType goodsTypeParent = goodsTypeDao.getGoodsTypeStateBypId(pId);
            if (goodsTypeParent.getGoodsTypeState() == 0) {
                goodsTypeParent.setGoodsTypeState(1);
                goodsTypeDao.updateGoodsTypeState(goodsTypeParent);
            }
        } else {
            serviceVO = new ServiceVO(ErrorCode.REQ_ERROR_CODE, ErrorCode.REQ_ERROR_MESS);
        }
        return serviceVO;
    }

    //删除分类
    @Override
    public ServiceVO delete(Integer goodsTypeId) {
        ServiceVO serviceVO;
        GoodsType goodsTypeNow = goodsTypeDao.selectByGoodsId(goodsTypeId);
        GoodsType goodsTypeParent = goodsTypeDao.getGoodsTypeStateBypId(goodsTypeNow.getPId());
        Integer i = goodsTypeDao.selectCountByPId(goodsTypeNow.getPId());
        int number = goodsTypeDao.delete(goodsTypeId);
        logService.save(new Log(Log.DELETE_ACTION, "删除分类"));
        if (number == 1) {
            serviceVO = new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
            if (i == 1) {
                if (goodsTypeParent.getGoodsTypeId() != 1) {
                    if (goodsTypeParent.getGoodsTypeState() == 1) {
                        goodsTypeParent.setGoodsTypeState(0);
                        goodsTypeDao.updateGoodsTypeState(goodsTypeParent);
                    }
                }
            }
        } else {
            serviceVO = new ServiceVO(ErrorCode.REQ_ERROR_CODE, ErrorCode.REQ_ERROR_MESS);
        }
        return serviceVO;

    }

    /**
     * 根据父菜单ID获取菜单
     *
     * @return
     */
    public JsonArray getGoodsTypeByParentId(Integer parentId, Integer roleId) {

        JsonArray array = new JsonArray();

        List<GoodsType> goodsTypes = goodsTypeDao.getGoodsTypeByParentId(parentId, roleId);

        //遍历菜单
        for (GoodsType goodsType : goodsTypes) {

            JsonObject obj = new JsonObject();

            obj.addProperty("id", goodsType.getGoodsTypeId());// 菜单ID

            obj.addProperty("text", goodsType.getGoodsTypeName());// 菜单名称

            obj.addProperty("iconCls", "goods-type");// 图标

            if (goodsType.getGoodsTypeState() == 1) {

                obj.addProperty("state", "closed"); // 根节点

            } else {

                obj.addProperty("state", "open");// 叶子节点
            }

            JsonObject attributes = new JsonObject(); //扩展属性

            attributes.addProperty("state", goodsType.getGoodsTypeState());

            obj.add("attributes", attributes);

            array.add(obj);

        }

        return array;
    }

    /**
     * 递归查询当前角色下的所有菜单
     *
     * @return
     */
    public JsonArray getAllMenu(Integer parentId, Integer roleId) {

        JsonArray array = this.getGoodsTypeByParentId(parentId, roleId);

        for (int i = 0; i < array.size(); i++) {

            JsonObject obj = (JsonObject) array.get(i);

            if (obj.get("state").getAsString().equals("open")) {//如果是叶子节点，不再递归

            } else {//如果是根节点，继续递归查询

                obj.add("children", this.getAllMenu(obj.get("id").getAsInt(), roleId));

            }

        }

        return array;
    }
}