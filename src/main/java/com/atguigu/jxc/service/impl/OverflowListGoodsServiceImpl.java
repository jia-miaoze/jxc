package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson.JSON;
import com.atguigu.jxc.dao.*;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.OverflowListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
//商品报溢
@Service
public class OverflowListGoodsServiceImpl implements OverflowListGoodsService {
    @Autowired
    private OverflowListDao overflowListDao;
    @Autowired
    private OverflowListGoodsDao overflowListGoodsDao;
    @Autowired
    private GoodsDao goodsDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private LogService logService;
    //    新增报溢单
    @Override
    public ServiceVO save(OverflowList overflowList, String overflowListGoodsStr) {
        ServiceVO serviceVO=null;
        String overflowNumber = overflowList.getOverflowNumber();
        String overflowDate = overflowList.getOverflowDate();
        String remarks = overflowList.getRemarks();
        overflowList.setUserId(1);
        Integer userId = overflowList.getUserId();
        int  overflowListNumber=overflowListDao.save(overflowNumber,overflowDate,remarks,userId);
        logService.save(new Log(Log.INSERT_ACTION, "增加damageList"));
        if (overflowListNumber==1){
            serviceVO = new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
        } else {
            return new ServiceVO(ErrorCode.REQ_ERROR_CODE, ErrorCode.REQ_ERROR_MESS);
        }
        Integer  overflowListId=overflowListDao.selectOne(overflowNumber,overflowDate,remarks,userId);
        List<Map> mapList = JSON.parseArray(overflowListGoodsStr, Map.class);
        if (!CollectionUtils.isEmpty(mapList)){
            //  循环遍历
            for (Map map : mapList) {
                Integer goodsId = (Integer) map.get("goodsId");
                String goodsCode = (String) map.get("goodsCode");
                String goodsName = (String) map.get("goodsName");
                String goodsModel = (String) map.get("goodsModel");
                String goodsUnit = (String) map.get("goodsUnit");
                Double lastPurchasingPrice=Double.parseDouble(String.valueOf(map.get("lastPurchasingPrice")));
                Double price=Double.parseDouble(String.valueOf(map.get("price")));
                Integer goodsNum=Integer.parseInt(String.valueOf(map.get("goodsNum")));
                Double total=Double.parseDouble(String.valueOf(map.get("total")));
                Goods goods = goodsDao.selectById(goodsId);
                Integer goodsTypeId = goods.getGoodsTypeId();
                int overflowListGoodsNumber =overflowListGoodsDao.save(goodsId,goodsCode,goodsName,goodsModel,goodsUnit,goodsNum,price,total,overflowListId,goodsTypeId);
                logService.save(new Log(Log.INSERT_ACTION, "增加overflowListGoods"));
                if (overflowListGoodsNumber==1){
                    serviceVO = new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
                } else {
                    return new ServiceVO(ErrorCode.REQ_ERROR_CODE, ErrorCode.REQ_ERROR_MESS);
                }
                int updateNumber=goodsDao.updateLastPurchasingPrice(lastPurchasingPrice,goodsId);
                logService.save(new Log(Log.INSERT_ACTION, "updateLastPurchasingPrice"));
                if (updateNumber==1){
                    serviceVO = new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
                } else {
                    return new ServiceVO(ErrorCode.REQ_ERROR_CODE, ErrorCode.REQ_ERROR_MESS);
                }
            }
        }

        return serviceVO;
    }
    //报溢单查询
    @Override
    public Map<String, Object> list(String sTime, String eTime) {
        HashMap<String, Object> map = new HashMap<>();
        ArrayList<OverflowList> overflowLists = new ArrayList<>();
        List<OverflowList> overflowListAll=overflowListDao.selectByDate(sTime,eTime);
        for (OverflowList overflowList : overflowListAll) {
            User userById = userDao.getUserById(overflowList.getUserId());
            overflowList.setTrueName(userById.getTrueName());
            overflowLists.add(overflowList);
        }
        map.put("rows",overflowLists);
        logService.save(new Log(Log.SELECT_ACTION, "报溢单查询"));
        return map;
    }
    //报溢单商品信息
    @Override
    public Map<String, Object> goodsList(Integer overflowListId) {
        HashMap<String, Object> map = new HashMap<>();
        List<OverflowListGoods> overflowListGoodsList=overflowListGoodsDao.selectById(overflowListId);
        map.put("rows", overflowListGoodsList);
        logService.save(new Log(Log.SELECT_ACTION, "查询报损单商品信息"));
        //damageListGoodsDao
        return map;
    }
}
