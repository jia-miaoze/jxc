package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.entity.Role;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SupplierService;

import com.atguigu.jxc.util.DateUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    SupplierDao supplierDao;

    @Autowired
    LogService logService;

    @Override
    public Map<String, Object> list(Integer page, Integer rows, String supplierName) {

        Map<String, Object> map = new HashMap<>();

        int total = supplierDao.getSupplierCount(supplierName);

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        List<Supplier> suppliers = supplierDao.getSupplierList(offSet, rows, supplierName);

        logService.save(new Log(Log.SELECT_ACTION, "分页查询供应商信息"));

        map.put("total", total);
        map.put("rows", suppliers);

        return map;

    }

    @Override
    public ServiceVO saveSupplier(Supplier supplier , String supplierId) {
        //判断 是否存在id
        if (supplierId == null){
            // id 不存在，新增！
            // 判断供应商名称是否存在，防止重复添加！
            Supplier byName = supplierDao.findByName(supplier.getSupplierName());
            if (byName != null){
                // 数据存在则返回提示信息！
                return new ServiceVO(ErrorCode.ROLE_EXIST_CODE, ErrorCode.ROLE_EXIST_MESS);
            }

            // 数据不在，则添加新的数据！
            logService.save(new Log(Log.INSERT_ACTION, "新增供应商:" + supplier.getSupplierName()));
            supplierDao.insertSupplier(supplier);
        }else {
            // 更新数据！
            logService.save(new Log(Log.INSERT_ACTION, "修改供应商:" + supplier.getSupplierName()));
            supplierDao.updateSupplier(supplier);
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }


    @Override
    public void deleteSupplierById(String ids) {

        // 删除供应商！
        String[] split = ids.split(",");

        supplierDao.deleteSupplierById(split);
    }
}
