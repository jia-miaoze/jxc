package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.dao.UnitDao;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UnitServiceImpl implements UnitService {
    @Autowired
    private UnitDao unitDao;
    @Autowired
    private LogService logService;
    //    查询所有商品单位
    @Override
    public Map<String, Object> list() {
        HashMap<String, Object> map = new HashMap<>();

        List<Unit> nuitList=unitDao.list();
        map.put("rows", nuitList);
        logService.save(new Log(Log.SELECT_ACTION, "客户列表分页"));

        return map;
    }
}
